# OpenML dataset: LEV

https://www.openml.org/d/1029

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

1. Title: Lecturers Evaluation (Ordinal LEV)

2. Source Informaion:
   Donor: Arie Ben David
          MIS, Dept. of Technology Management 
          Holon Academic Inst. of Technology
          52 Golomb St.
          Holon 58102
          Israel
          abendav@hait.ac.il
   Owner: Yoav Ganzah
          Business Administration School
          Tel Aviv Univerity
          Ramat Aviv 69978
          Israel

3. Past Usage:

4. Relevant Information 
The LEV data set contains examples of anonymous lecturer evaluations, 
taken at the end of MBA courses. Before receiving the final grades, students
were asked to score their lecturers according to four attributes such as 
oral skills and contribution to their professional/general knowledge. 
The single output was a total evaluation of the lecturer’s performance.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1029) of an [OpenML dataset](https://www.openml.org/d/1029). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1029/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1029/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1029/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

